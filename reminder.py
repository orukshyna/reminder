import time

from plyer import notification


def reminder():
    iterations = 0
    print("The notification is set.")
    while iterations <= 5:
        iterations += 1
        notification.notify(title=f"Iteration number {iterations}",
                            message="The next exercise.")
        time.sleep(5)
    print("The exercises are completed.")


if __name__ == "__main__":
    reminder()
